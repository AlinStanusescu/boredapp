//
//  APIClient.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 27.10.2021.
//

import Alamofire
import Foundation

class APIClient {
    static var shared = APIClient()
  
    func fetchActivities(completionHandler: @escaping (ActivityModel?) -> Void,
                         errorHandler: @escaping (Error) -> Void) {
        
        let url =  APIRouter.fetchActivities.asURLRequest()
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    guard let data = response.data else { return }
                    
                    do {
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(ActivityModel.self, from: data)
                        completionHandler(model)
                    } catch let error {
                        print(error)
                        completionHandler(nil)
                    }
                    
                case .failure(let error):
                    print(error)
                    errorHandler(error)
                }
            })
        
    }
    
}




