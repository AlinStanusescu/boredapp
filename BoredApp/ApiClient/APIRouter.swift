//
//  APIRouter.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 13.01.2022.
//

import Foundation

enum APIRouter  {

    case fetchActivities
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .fetchActivities:
            return "api/activity/"
        }
    }
    
    // MARK: - URL
    func asURLRequest() ->  URL {
        let url : URL?
        
        switch self {
        case .fetchActivities:
            url =  URL.init(string: Constants.ProductionServer.baseURL + self.path)!
        }
        
        return url!
    }
}
