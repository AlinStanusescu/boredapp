//
//  Constants.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 13.01.2022.
//

import Foundation

struct Constants {

    struct ProductionServer {
        static let baseURL = "https://www.boredapi.com/"
    }
}

