//
//  ViewController.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 13.01.2022.
//

import UIKit

class ViewController: UIViewController {
    
    var viewModel = ViewModel()
    @IBOutlet weak var loadingView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func loadData() {
        self.loadingView.isHidden = false
        self.viewModel.fetchActivities { model in
            self.loadingView.isHidden = true
            guard let model = model else {
                return
            }
            
            let alert = UIAlertController(title: "Activity Name", message: self.viewModel.activityName, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Details", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
                self.goToDetailsScreen(model: model)
            }))
            self.present(alert, animated: true, completion: nil)
            
        } errorHandler: { error in
            self.loadingView.isHidden = true
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Actions
    @IBAction func buttonTapped(sender: Any) {
        self.loadData()
    }
    
    private func goToDetailsScreen(model: ActivityModel) {
        self.presentActivityDetailsScreen(model: model)
    }
}
