//
//  ViewController+Navigation.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 13.01.2022.
//

import Foundation

extension ViewController {
    
    func presentActivityDetailsScreen(model: ActivityModel) {
        let controller = DetailsScreenViewController.createController(model: model)
        self.present(controller, animated: true, completion: nil)
    }
}
