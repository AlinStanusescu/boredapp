//
//  ViewModel.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 13.01.2022.
//

import Foundation

class ViewModel: NSObject {
    
    var model: ActivityModel?
    
    var activityName: String {
        get {
            return model?.activity ?? ""
        }
    }
    
    func fetchActivities(completionHandler: @escaping (ActivityModel?) -> Void,
                         errorHandler: @escaping (Error) -> Void) {
        
        APIClient.shared.fetchActivities { activityModel in
            guard let model = activityModel else {
                completionHandler(nil)
                return
            }
            
            self.model = model
            completionHandler(model)
        } errorHandler: { error in
            errorHandler(error)
        }
    }
    
    //test sourcetree
}
