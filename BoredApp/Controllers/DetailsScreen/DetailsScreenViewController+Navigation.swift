//
//  DetailsScreenViewController+Navigation.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 13.01.2022.
//


import UIKit

extension DetailsScreenViewController {
    
    /// Build controller
    class func createController(model: ActivityModel) -> DetailsScreenViewController {
        let controller = DetailsScreenViewController.create()
        controller.viewModel = DetailsScreenViewModel(model: model)
        return controller
    }
}
