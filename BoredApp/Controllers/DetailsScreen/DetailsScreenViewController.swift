//
//  DetailsScreenViewController.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 13.01.2022.
//

import UIKit
import AVFoundation

class DetailsScreenViewController: UIViewController {
    
    var viewModel : DetailsScreenViewModel!
    
    @IBOutlet weak var detailsScreenLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.loadScreenDetails()
    }
    
    private func setup() {
        self.detailsScreenLabel.font = UIFont.boldSystemFont(ofSize: 17)
        self.detailsScreenLabel.textColor = UIColor.white
    }
    
    private func loadScreenDetails() {
        self.detailsScreenLabel.text = self.viewModel.prepareScreenDetails()
        
        self.viewModel.prepareScreenColorAndSond { color, soundId in
            self.view.backgroundColor  = color
            AudioServicesPlayAlertSound(SystemSoundID(soundId))
        }
    }
}
