//
//  DetailsScreenViewModel.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 13.01.2022.
//


import Foundation
import UIKit

class DetailsScreenViewModel: NSObject {
    
    var model: ActivityModel
    
    private var title: String {
        return model.activity ?? ""
    }
    
    var priceDetails: String {
        if (self.model.price ?? 0.0) > 0.0 {
            return "Price: Paid"
        } else {
            return "Price: Free"
        }
    }
    
    private var noOfParticipants: String {
        return ("No of participants is \(model.participants ?? 0)")
    }
    
    init(model: ActivityModel) {
        self.model = model
    }
    
    
    func prepareScreenDetails() -> String {
        return ("\(self.title) \n \(self.priceDetails) \n \(self.noOfParticipants)")
    }
    
    func prepareScreenColorAndSond(setupHandler: @escaping (_ color: UIColor, _ soundId: Int) -> Void) {
        
        switch model.type  {
        case .recreational:
            setupHandler(UIColor.brown, 1322)
        case .social:
            setupHandler(UIColor.cyan, 1323)
        case .busywork:
            setupHandler(UIColor.green, 1324)
        case .charity:
            setupHandler(UIColor.gray, 1325)
        case .cooking:
            setupHandler(UIColor.orange, 1326)
        case .relaxation:
            setupHandler(UIColor.blue, 1327)
        case .education:
            setupHandler(UIColor.magenta, 1328)
        case .none:
            setupHandler(UIColor.darkGray, 1329)
        }
    }
}


