//
//  ActivityModel.swift
//  BoredApp
//
//  Created by Alin Stanusescu on 13.01.2022.
//

import Foundation

struct ActivityModel: Codable {
    
    var activity: String?
    var type: ActivityType?
    var participants: Int?
    var price: Float?
    var link: String?
    var key: String?
    var accessibility: Float?
}


enum ActivityType: String, Codable {
    case recreational = "recreational"
    case social = "social"
    case busywork = "busywork"
    case charity = "charity"
    case cooking = "cooking"
    case relaxation = "relaxation"
    case education = "education"
}

/**
 {
 "activity": "Play a game of tennis with a friend",
 "type": "social",
 "participants": 2,
 "price": 0.1,
 "link": "",
 "key": "1093640",
 "accessibility": 0.4
 }
 
 */

